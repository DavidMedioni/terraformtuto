# Bucket with unique name

resource "random_string" "bucket-name" {
  length  = 5
  upper   = false
  number  = false
  lower   = true
  special = false
}
resource "aws_s3_bucket" "s3bucket" {
  bucket = "datapred-${random_string.bucket-name.result}"
}
